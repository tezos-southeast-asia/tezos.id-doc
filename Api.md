# Api (On Developing)
By default, all request will return the latest 10 records (n=10 and p=0) and the maximum number of records that can be retrieved is 50.

To add a parameter to a request, use ?{parameter}={value}.


Example:

/v1/blocks?n=5

/v1/transactions?p=0&n=10


**API Endpoint:**
https://api.tezos.id/mooncake/mainnet


## /v1/blocks
Returns the blocks and its details on the chain.
### GET 200

### Parameters
- p: page
- n: number per page

## /v1/blocks/\<hash\>|\<level\>
Returns a specific block details on the chain by specifying the block hash or block level.
### GET 200

## /v1/blocks_num
Returns the total number of blocks.
### GET 200

## /v1/operations
Returns all operations and its details.


Types of operations

* Transaction: v1/operations?type=transactions
* Endorsements: v1/operations?type=endorsements
* Originations: v1/operations?type=originations
* Delegations: v1/operations?type=delegations


### GET 200

### Parameters
- p: page
- n: number per page

## /v1/operations/\<op_hash\>
Returns a specific operation details on the chain by specifying the operation hash.
### GET 200

## /v1/operations_num
Returns the total number of operations
### GET 200

## /v1/transactions
Returns the transactions and its details.

To retrieve a specific transaction operation details, use ?op={op_hash}

Example: v1/transactions?op={op_hash}


To retrieve a transaction operation detail by account name, use ?account={account}

Example: v1/transactions?account={account}


To retrieve a transaction operation detail by block, use ?block={hash}

Example: v1/transactions?block={hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/transactions_num
Returns the total number of transactions.
### GET 200

### Parameters
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/endorsements
Returns the endorsement operations and its details.

To retrieve a specific endorsements operation details, use ?op={op_hash}

Example: v1/endorsements?op={op_hash}

To retrieve endorsement operation detail by account name, use ?account={account}

Example: v1/endorsements?account={account}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/endorsements_num
Returns the total number of endorsement operations.
### GET 200

### Parameters
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/delegations
Returns the delegation operations and its details.

To retrieve a specific delegation operation, ?op={op_hash}

Example: v1/delegations?op={op_hash}
### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/delegations_num
Returns the total number of delegations operation.
### GET 200

### Parameters
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/originations
Returns the originations operations and its details.

To retrieve a specific originations operation, use ?op={op_hash}

Example: v1/originations?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/originations_num
Returns the total number of originations operations.
### GET 200

### Parameters
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/ballots
Returns the ballots operations and its details.
To retrieve a specific ballots operation details, use ?op={op_hash}

Example: v1/ballots?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`
- proposal: filter by proposal.

## /v1/ballots_num
Returns the total number of ballots operation.
### GET 200

### Parameters
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`
- proposal: filter by proposal.

## /v1/proposals
Returns the proposals operations and its details.

To retrieve a specific proposal operation details, use ?op={op_hash}

Example: v1/proposals?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/proposals_num
Returns the total number of proposals operation.
### GET 200

### Parameters
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/proposers
Returns the proposers operations and its details.

To retrieve a specific proposers operation details, use ?op={op_hash}

Example: v1/proposers?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page

## /v1/proposers_num
Returns the total number of proposers operation.
### GET 200

## /v1/activate_accounts
Returns the account activations operations and its details.

To retrieve a specific account activation operation details, use ?op={op_hash}

Example: v1/activate_accounts?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/activate_accounts_num
Returns the total number of account activation operations.
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/reveals
Returns the account reveals operations and its details.

To retrieve a specific account reveal operation details, use ?op={op_hash}

Example: v1/reveals?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/reveals_num
Returns the total number of account reveals operations.
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash
- account: filter by account. either `tz` or `KT`

## /v1/seed_nonce_revelations
Returns the seed nonce reveals operations and its details.

To retrieve a specific seed nonce revelation operation details, use ?op={op_hash}

Example: v1/seed_nonce_revelation?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/seed_nonce_revelations_num
Returns the total number of seed nonce revelations operations.
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/protocals
Returns the protocols operation and its details.

To retrieve a specific protocol operation details, use ?op={op_hash}

Example: v1/protocals?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page

## /v1/protocals_num
Returns the total number of protocols operation.
### GET 200

## /v1/double_baking_evidences
Returns the double baking evidence operations and its details.

To retrieve a specific double baking evidence operation details, use ?op={op_hash}

Example: v1/double_baking_evidences?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/double_baking_evidences
Returns the total number of double baking evidence operations.
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/double_endorsement_evidences
Returns the double endorsement evidence operations and its details.

To retrieve a specific double endorsement evidence operation details, use ?op={op_hash}

Example: v1/double_endorsement_evidences?op={op_hash}

### GET 200

### Parameters
- p: page
- n: number per page
- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/double_endorsement_evidences_num
Returns the total number of double endorsement evidence operations.
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/24_hours_stat
Returns the total number of blocks and transactions in the last 24 hours.
### GET 200

- block: can be filtered `level` or `hash` of block
- op: filter by op_hash

## /v1/accounts
Returns the tz1 and kt accounts.
### GET 200

### Parameters
- p: page
- n: number per page
- prefix: {tz, tz1, tz2, tz3 ,KT1 etc}

## /v1/accounts_num
Returns the total number of accounts.
### GET 200
- prefix: {tz, tz1, tz2, tz3 ,KT1 etc}

